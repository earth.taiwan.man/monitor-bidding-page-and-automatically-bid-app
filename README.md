# 監控 標案頁面 與 自動投標 應用程式

## 此程式的多種性質
- 自動化
- 網頁監控 (主要透過 Selenium 套件)
- 網路爬蟲
- 資料分析

## 此程式的檔案架構
- 資料夾 source_codes_v20
    * bid_header.py
    * global_variables.py
    * settings.py
    * images.py
    * table_layout.py
    * workbook_access.py
    * network_situations.py
    * notice.py
    * page_cares.py
    * manage_rows.py
    * bid_automations.py
    * main.py

- 資料夾 basic_settings
    * 單一設置檔

- 資料夾 excel_files
    * 一些Excel活頁簿檔案

- 資料夾 images
    * 一些「存放不同群組之影像」的資料夾

- 資料夾 audios
    * 一些聲音檔

- 資料夾 chrome_related
    * 資料夾 chromedriver-win64
    * 資料夾 profile

## 關於 bid_header.py
內含一堆「import ...」和「from ... import ...」的語法。

## 關於 global_variables.py
內含整個程式所需之絕大多數的變數宣告。

## 關於 settings.py
專門用來處理「監控標案頁面」和「自動投標」的相關基本設置。例如：
- 存放各種檔案的工作資料夾
- 標案頁面的自動刷新之週期時間
- Email通知和聲音通知的時間點
- 自動投標的時間點
- 登入特定標案或拍賣等網站的帳號和其密碼

## 關於 images.py
專門用來處理商品影像檔的檢測、下載、儲存和顯示。

## 關於 table_layout.py
專門用來處理標案項目的新增、修改、刪除、查看最高出價金額、查看目前參與投標者等等使用者互動的機制。

## workbook_access.py
專門用來處理標案項目之載入、儲存至Excel檔案的相關機制。

## network_situations.py
專門用來偵測目前網路上傳和下載速度與判斷網址正確性。

## notice.py
專門用來處理Email通知和聲音通知的機制。

## page_cares.py
專門用來操控監控型瀏覽器，以處理非常複雜的登入機制、各標案頁面的連線、監視、刷新、重設、關閉等等機制。

## manage_rows.py
專門用來處理在各標案頁面當中，極度複雜的資料擷取 (爬蟲)、剩餘時間更新、判定標案所進入的階段 (最後通知、循環更新、最後倒數、自動投標、標案結束等等) 等等機制。

## bid_automations.py
專門用來處理至關重要的最後通知、自動投標、訊息告知等等自動化機制。

## main.py
專門用來生成使用者可互動操作的視窗介面。

**由於此程式，牽涉到 營業祕密；所以，在此並不適合提供其程式碼...。**
